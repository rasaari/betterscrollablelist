﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BetterLayout : MonoBehaviour {
	public enum Direction {
		Vertical,
		Horizontal
	}

	[SerializeField] Direction _direction;
	[SerializeField] float _padding;

	RectTransform _transform;

	void Awake () {
		_transform = GetComponent<RectTransform> ();
	}

	void FitContent () {
		if (_transform == null) {
			_transform = GetComponent<RectTransform> ();
		}

		float totalHeight = 0.0f;
		float items = transform.childCount;

		foreach (Transform child in transform) {
			float height = child.GetComponent <RectTransform> ().sizeDelta.y;
			totalHeight += height;
			totalHeight += _padding;
		}
			
		_transform.sizeDelta = new Vector2 (100, totalHeight);

		int i = 0;
		foreach (Transform child in transform) {
			//child.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, (totalHeight / items * i) - totalHeight / 2);
			Vector2 anchor = new Vector2 (0.5f, (i + 1) / items);

			child.GetComponent<RectTransform> ().anchorMin = anchor;
			child.GetComponent<RectTransform> ().anchorMax = anchor;
			i++;
		}
	}

	void Update () {
		FitContent ();
	}
}
